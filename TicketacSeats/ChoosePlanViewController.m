//
//  ViewController.m
//  TicketacSeats
//
//  Created by Konrad Kołakowski on 05/09/2019.
//  Copyright © 2019 Le Figaro. All rights reserved.
//

#import "ChoosePlanViewController.h"
#import "TTSeatsPlanViewController.h"

#define kSpectacleIdTextFieldTag 100
#define kRepresentationIdTextFieldTag 101
#define kOriginIdTextFieldTag 102

@interface ChoosePlanViewController ()

@property (weak, nonatomic) IBOutlet UITextField *spectacleIdTextField;
@property (weak, nonatomic) IBOutlet UITextField *representationIdTextField;
@property (weak, nonatomic) IBOutlet UITextField *originTextField;

@property (weak, nonatomic) IBOutlet UIButton *openPlanButton;
@property (weak, nonatomic) IBOutlet UIView *openPlanContentView;

@end

@implementation ChoosePlanViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self validateFormData];
}

#pragma mark Helpers
- (BOOL)isDataValid {
    NSInteger spectacleId = [self.spectacleIdTextField.text integerValue];
    NSInteger representationId = [self.representationIdTextField.text integerValue];
    NSString* origin = [self.originTextField text];
    
    BOOL validated = NO;
    if (spectacleId > 0 && representationId > 0 && origin != nil) {
        if ([origin isEqualToString:@"aparte"] || [origin isEqualToString:@"rodrigue"]) {
            validated = YES;
        }
    }
    
    return validated;
}

- (void)validateFormData {
    BOOL validated = [self isDataValid];
    
    self.openPlanButton.enabled = validated;
    self.openPlanContentView.backgroundColor = validated ? [UIColor systemBlueColor]
                                                         : [UIColor systemGrayColor];
}

- (NSString*)seatsPlanFilePathFromSpectacleId:(NSInteger)spectacleId representationId:(NSInteger)representationId origin:(NSString*)origin {
    NSString* planName = [NSString stringWithFormat:@"plan_%ld_%ld_%@",
                          spectacleId, representationId, origin];
    return [[NSBundle mainBundle] pathForResource: planName ofType: @"json"];
}

- (NSDictionary*)loadAndParseSeatsPlanFromSpectacleId:(NSInteger)spectacleId representationId:(NSInteger)representationId origin:(NSString*)origin {
    NSString* planFilePath = [self seatsPlanFilePathFromSpectacleId: spectacleId
                                                   representationId: representationId
                                                             origin: origin];
    
    if (planFilePath != nil) {
        NSData* planFileData = [NSData dataWithContentsOfFile: planFilePath];
        if (planFileData != nil) {
            NSDictionary* plan = [NSJSONSerialization JSONObjectWithData:planFileData
                                                                 options: 0
                                                                   error: nil];
            
            return plan;
        }
    }
    
    return nil;
}

#pragma mark Actions

- (IBAction)textFieldValueChanged:(UITextField *)sender {
    [self validateFormData];
}

- (IBAction)openButtonTapped:(id)sender {
    NSDictionary* plan = [self loadAndParseSeatsPlanFromSpectacleId: [self.spectacleIdTextField.text integerValue]
                                                   representationId: [self.representationIdTextField.text integerValue]
                                                             origin: [self.originTextField text]];
    
    if (plan != nil) {
        TTSeatsPlanViewController* seatsVC = [[TTSeatsPlanViewController alloc] initWithNibName: @"TTSeatsPlanViewController" bundle: nil];
        seatsVC.plan = plan;
        
        [self showViewController: seatsVC sender: self];
    } else {
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Wrong Data"
                                                                       message:@"Cannot find JSON file for plan with those parameters"
                                                                preferredStyle: UIAlertControllerStyleAlert];
        [alert addAction: [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler: nil]];
        
        [self presentViewController: alert animated: true completion: nil];
    }
}

@end
