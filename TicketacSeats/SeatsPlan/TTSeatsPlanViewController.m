//
//  TTSeatsPlanViewController.m
//  TicketacSeats
//
//  Created by Konrad Kołakowski on 06/09/2019.
//  Copyright © 2019 Le Figaro. All rights reserved.
//

#import "UIColor+HexColors.h"

#import "TTSeatsPlanViewController.h"
#import "TTSeatsPlanLegendViewController.h"
#import "TTSelectedSeatsViewController.h"
#import "TTSeatsPlanView.h"

@interface TTSeatsPlanViewController ()

@property (strong, nonatomic) TTSeatsPlanView* planView;
@property (weak, nonatomic) IBOutlet UIScrollView *planScrollView;

@property (weak, nonatomic) IBOutlet UIButton *reserveButton;
@property (weak, nonatomic) IBOutlet UIView *reserveButtonDisableView;

@property (strong, nonatomic) TTSeatsPlanLegendViewController* legendViewController;
@property (strong, nonatomic) TTSelectedSeatsViewController* selectedSeatsViewController;

@property (strong, nonatomic) NSMutableArray<TTSeat*>* selectedSeats;

@end

@implementation TTSeatsPlanViewController

#pragma mark Initialization & overrides
- (void)viewDidLoad {
    [super viewDidLoad];
 
    self.selectedSeats = [NSMutableArray array];
    
    NSArray* tariffs = [self allTarrifsFromCategories: self.plan[@"categories"]];
     
    // legend view controller
    self.legendViewController = [[TTSeatsPlanLegendViewController alloc] initWithNibName:@"TTSeatsPlanLegendViewController"
                                                                                  bundle:nil];
    self.legendViewController.tariffs = tariffs;
    self.legendViewController.view.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.view addSubview: self.legendViewController.view];
    
    [NSLayoutConstraint activateConstraints: @[
            [self.legendViewController.view.leadingAnchor constraintEqualToAnchor: self.view.leadingAnchor],
            [self.legendViewController.view.trailingAnchor constraintEqualToAnchor: self.view.trailingAnchor],
            [self.legendViewController.view.topAnchor constraintEqualToAnchor: self.view.safeAreaLayoutGuide.topAnchor]
        ]
    ];
    
    [self addChildViewController: self.legendViewController];
    [self.legendViewController didMoveToParentViewController: self];
    
    // seats plan view (embedded in scroll view)
    self.planView = [[TTSeatsPlanView alloc] initWithFrame: CGRectZero];
    self.planView.seats = [self seatsFromPlan: self.plan tariffs: tariffs];
    self.planView.texts = [self textsFromPlan: self.plan];
    self.planView.delegate = self;
    
    [self.planScrollView addSubview: self.planView];
    self.planScrollView.delegate = self;
    self.planScrollView.contentSize = self.planView.frame.size;
    
    // selected seats view controller
    self.selectedSeatsViewController = [[TTSelectedSeatsViewController alloc] initWithNibName:@"TTSelectedSeatsViewController"
                                                                                        bundle:nil];
    self.selectedSeatsViewController.seats = self.selectedSeats;
    self.selectedSeatsViewController.view.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.view addSubview: self.selectedSeatsViewController.view];
    
    [NSLayoutConstraint activateConstraints: @[
            [self.selectedSeatsViewController.view.leadingAnchor constraintEqualToAnchor: self.view.leadingAnchor],
            [self.selectedSeatsViewController.view.trailingAnchor constraintEqualToAnchor: self.view.trailingAnchor],
            [self.selectedSeatsViewController.view.bottomAnchor constraintEqualToAnchor: self.reserveButton.topAnchor]
        ]
    ];
    
    [self addChildViewController: self.selectedSeatsViewController];
    [self.selectedSeatsViewController didMoveToParentViewController: self];
    
    // reserve button control
    [self setReserveButtonEnabled: NO];
}

#pragma mark Helpers
+ (NSArray*) seatsColorPallette {
    return @[
        [UIColor colorWithHexString: @"#F6D000"], [UIColor colorWithHexString: @"#D83642"],
        [UIColor colorWithHexString: @"#A18CED"], [UIColor colorWithHexString: @"#1AB696"],
        [UIColor colorWithHexString: @"#063F77"], [UIColor colorWithHexString: @"#CE115E"],
        [UIColor colorWithHexString: @"#F7747B"], [UIColor colorWithHexString: @"#721735"],
        [UIColor colorWithHexString: @"#27CAD1"], [UIColor colorWithHexString: @"#F67BAB"]
    ];
}

- (CGFloat) planScaleFactorFromPlan:(NSDictionary*)plan {
    NSArray* seatsArray = plan[@"seats"]; // we assume scale with seats is the same as with text
    
    // calculate all distances between adjacent seats
    CGPoint lastSeatPos = CGPointZero;
    NSMutableArray<NSNumber*>* dists = [NSMutableArray arrayWithCapacity: seatsArray.count];
    for (NSDictionary* seatDict in seatsArray) {
        CGPoint seatPos = CGPointMake([seatDict[@"x"] doubleValue], [seatDict[@"y"] doubleValue]);
        
        if (!CGPointEqualToPoint(seatPos, lastSeatPos)) {
            CGFloat dist = sqrt( ((seatPos.x - lastSeatPos.x)*(seatPos.x - lastSeatPos.x)) +
                                 ((seatPos.y - lastSeatPos.y)*(seatPos.y - lastSeatPos.y)) );
            [dists addObject: [NSNumber numberWithDouble: dist]];
            
            lastSeatPos = seatPos;
        }
    }
    
    // get median distance from all seats and it'll be our base for scaling
    [dists sortUsingComparator: ^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        double dist1 = [obj1 doubleValue];
        double dist2 = [obj2 doubleValue];
        
        if (dist1 < dist2) {
            return NSOrderedAscending;
        } else if (dist1 == dist2) {
            return NSOrderedSame;
        } else {
            return NSOrderedDescending;
        }
    }];
    
    const CGFloat targetSeatSize = 20.0;
    NSNumber* midElement = dists[dists.count / 2];
    
    return targetSeatSize / [midElement doubleValue];
}

- (CGPoint) planOffsetFromPlan:(NSDictionary*)plan {
    NSArray* seatsArray = plan[@"seats"];
    
    // calculate min and max seats position
    CGFloat minX = FLT_MAX;
    CGFloat minY = FLT_MAX;
    for (NSDictionary* seatDict in seatsArray) {
        CGFloat seatX = [seatDict[@"x"] doubleValue];
        CGFloat seatY = [seatDict[@"y"] doubleValue];
        
        // x
        if (seatX < minX) {
            minX = seatX;
        }
        
        // y
        if (seatY < minY) {
            minY = seatY;
        }
    }
    
    return CGPointMake(minX, minY);
}

- (NSArray<TTSeat*>*) seatsFromPlan:(NSDictionary*)plan tariffs:(NSArray*)tariffs {
    CGFloat scale = [self planScaleFactorFromPlan: plan];
    CGPoint offset = [self planOffsetFromPlan: plan];
    offset = CGPointApplyAffineTransform(offset, CGAffineTransformMakeScale(scale, scale));
    offset = CGPointApplyAffineTransform(offset, CGAffineTransformMakeTranslation(-kPlanMargins, -kPlanMargins));
    
    NSArray* seatsArray = plan[@"seats"];
    NSMutableArray<TTSeat*>* result = [NSMutableArray arrayWithCapacity: seatsArray.count];
    for (NSDictionary* seatDict in seatsArray) {
        CGFloat seatX = [seatDict[@"x"] doubleValue] * scale - offset.x;
        CGFloat seatY = [seatDict[@"y"] doubleValue] * scale - offset.y;
        
        NSMutableDictionary* transformedSeatDict = [NSMutableDictionary dictionaryWithDictionary: seatDict];
        transformedSeatDict[@"x"] = [NSNumber numberWithDouble: seatX];
        transformedSeatDict[@"y"] = [NSNumber numberWithDouble: seatY];
        
        TTSeat* seat = [[TTSeat alloc] initWithDict:transformedSeatDict tariffs:tariffs];
        if (seat) {
            [result addObject: seat];
        }
    }
    
    return result;
}

- (NSArray<TTText*>*) textsFromPlan:(NSDictionary*)plan {
    CGFloat scale = [self planScaleFactorFromPlan: plan];
    CGPoint offset = [self planOffsetFromPlan: plan];
    offset = CGPointApplyAffineTransform(offset, CGAffineTransformMakeScale(scale, scale));
    offset = CGPointApplyAffineTransform(offset, CGAffineTransformMakeTranslation(-kPlanMargins, -kPlanMargins));
    
    NSArray* textsArray = plan[@"texts"];
    NSMutableArray<TTText*>* result = [NSMutableArray arrayWithCapacity: textsArray.count];
    for (NSDictionary* textDict in textsArray) {
        CGFloat textX = [textDict[@"x"] doubleValue] * scale - offset.x;
        CGFloat textY = [textDict[@"y"] doubleValue] * scale - offset.y;
        
        NSMutableDictionary* transformedTextDict = [NSMutableDictionary dictionaryWithDictionary: textDict];
        transformedTextDict[@"x"] = [NSNumber numberWithDouble: textX];
        transformedTextDict[@"y"] = [NSNumber numberWithDouble: textY];
        
        TTText* text = [[TTText alloc] initWithDict: transformedTextDict];
        if (text) {
            [result addObject: text];
        }
    }
    
    return result;
}


- (NSArray*) allTarrifsFromCategories:(NSDictionary*)categories {
    NSMutableArray* result = [NSMutableArray arrayWithCapacity: categories.allKeys.count]; // at least so many tariffs
    
    for (NSDictionary* category in categories.allValues) {
        NSDictionary* tariffs = category[@"tarifs"];
        for (NSDictionary* tariff in tariffs.allValues) {
            [result addObject: [NSMutableDictionary dictionaryWithDictionary: tariff]];
        }
    }
    
    // sort by prices
    [result sortUsingComparator: ^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        float price1 = [obj1[@"tarif"] floatValue];
        float price2 = [obj2[@"tarif"] floatValue];
        
        if (price1 < price2) {
            return NSOrderedAscending;
        } else if (price1 == price2) {
            return NSOrderedSame;
        } else {
            return NSOrderedDescending;
        }
    }];
    
    // assign colors, in a loop
    NSArray* colors = [TTSeatsPlanViewController seatsColorPallette];
    NSInteger i = 0;
    for (NSMutableDictionary* tariff in result) {
        tariff[@"color"] = colors[i];
        
        if (++i == colors.count) {
            i = 0;
        }
    }
    
    return result;
}

- (void) setReserveButtonEnabled:(BOOL)enabled {
    self.reserveButton.enabled = enabled;
    self.reserveButtonDisableView.hidden = enabled;
}

#pragma mark Actions

- (IBAction)reserve:(id)sender {
    // TODO: Implement, but I think it'll be in Ticketac project proper
    NSLog(@"Will reserve %ld tickets!", self.selectedSeats.count);
}

#pragma mark UIScrollViewDelegate implementation

-(UIView*) viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return self.planView;
}

#pragma mark TTSeatsPlanViewDelegate implementation

-(void) seatsPlanDidSelectSeat:(TTSeat*)seat {
    [self.selectedSeats addObject: seat];
    self.selectedSeatsViewController.seats = self.selectedSeats;
    
    NSLog(@"SEAT SELECTED: (%.2f,%.2f)", seat.x, seat.y);
    
    [self setReserveButtonEnabled: self.selectedSeats.count > 0];
}

-(void) seatsPlanDidDeselectSeat:(TTSeat*)seat {
    [self.selectedSeats removeObject: seat];
    self.selectedSeatsViewController.seats = self.selectedSeats;
    
    [self setReserveButtonEnabled: self.selectedSeats.count > 0];
}

@end
