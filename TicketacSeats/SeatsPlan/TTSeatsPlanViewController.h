//
//  TTSeatsPlanViewController.h
//  TicketacSeats
//
//  Created by Konrad Kołakowski on 06/09/2019.
//  Copyright © 2019 Le Figaro. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TTSeatsPlanView.h"

NS_ASSUME_NONNULL_BEGIN

@interface TTSeatsPlanViewController : UIViewController <UIScrollViewDelegate, TTSeatsPlanViewDelegate>

@property (nonatomic, strong) NSDictionary* plan;

@end

NS_ASSUME_NONNULL_END
