//
//  TTSeatsPlanView.m
//  TicketacSeats
//
//  Created by Konrad Kołakowski on 05/09/2019.
//  Copyright © 2019 Le Figaro. All rights reserved.
//

#import "TTSeatsPlanView.h"

@interface TTSeatsPlanView()

@property (nonatomic, weak) TTSeat* previouslyTouchedSeat;

@property (nonatomic, strong) UIImage* seatPersonImage;
@property (nonatomic, strong) UIFont* textsFont;

@end

@implementation TTSeatsPlanView

#pragma mark Initialization & overrides

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.previouslyTouchedSeat = nil;
        self.seatPersonImage = [UIImage imageNamed: @"seat_person"];
        self.textsFont = [UIFont systemFontOfSize: 14];
        
        self.backgroundColor = UIColor.clearColor;
        self.multipleTouchEnabled = NO;
    }
    
    return self;
}

- (void)drawRect:(CGRect)rect {
    CGContextRef ctx = UIGraphicsGetCurrentContext();

    CGColorRef whiteColor = UIColor.whiteColor.CGColor;
    CGColorRef highlightColor = CGColorCreateCopyWithAlpha(whiteColor, 0.7);
    CGColorRef blackStrokeColor = UIColor.blackColor.CGColor;

    NSMutableParagraphStyle* textsStyle = [[NSMutableParagraphStyle alloc] init];
    textsStyle.alignment = NSTextAlignmentCenter;
    
    NSDictionary<NSAttributedStringKey,id>* textsAttrs = @{
        NSFontAttributeName: self.textsFont,
        NSForegroundColorAttributeName: UIColor.blackColor,
        NSParagraphStyleAttributeName: textsStyle
    };
    
    // draw all texts
    for (TTText* text in self.texts) {
        CGRect textRect = CGRectMake(text.x, text.y, kTextRectSize, kTextRectSize);
        
        [text.text drawWithRect: textRect
                        options: NSStringDrawingUsesLineFragmentOrigin
                     attributes: textsAttrs
                        context: nil];
    }
    
    // draw all seats for a proper color
    for (TTSeat* seat in self.seats) {
        CGRect seatRect = CGRectMake(seat.x, seat.y, kSeatRectSize, kSeatRectSize);
        
        CGContextSetFillColorWithColor(ctx, seat.color.CGColor);
        CGContextSetStrokeColorWithColor(ctx, blackStrokeColor);
        CGContextFillRect(ctx, seatRect);
        CGContextStrokeRect(ctx, seatRect);
        
        // draw a higlight if highlighted (just before selection)
        if (seat.highlighted) {
            CGContextSetFillColorWithColor(ctx, highlightColor);
            CGContextFillRect(ctx, seatRect);
            CGContextStrokeRect(ctx, seatRect);
        }
        
        // draw "dot" if selected
        if (seat.selected) {
            const CGFloat kHeadSize = 8.0;
            
            CGContextSetFillColorWithColor(ctx, whiteColor);
            CGContextDrawImage(ctx, CGRectMake(seat.x + (kSeatRectSize - kHeadSize)/2.0,
                                               seat.y + (kSeatRectSize - kHeadSize)/2.0,
                                               kHeadSize, kHeadSize),
                               self.seatPersonImage.CGImage);
        }
    }
}

- (void)touchesBegan:(NSSet<UITouch*> *)touches withEvent:(UIEvent*)event {
    self.previouslyTouchedSeat.highlighted = NO;
    self.previouslyTouchedSeat = nil;
    
    for (UITouch* touch in touches) {
        CGPoint position = [touch locationInView: self];
        TTSeat* seat = [self seatForPosition: position];
        if (seat != nil) {
            seat.highlighted = YES;
            self.previouslyTouchedSeat = seat;
        }
    }
    
    [self setNeedsDisplay];
}

- (void)touchesMoved:(NSSet<UITouch*> *)touches withEvent:(UIEvent*)event {
    for (UITouch* touch in touches) {
        CGPoint position = [touch locationInView: self];
        TTSeat* seat = [self seatForPosition: position];
        if (seat != nil) {
            if (seat != self.previouslyTouchedSeat) {
                self.previouslyTouchedSeat.highlighted = NO;
                
                seat.highlighted = YES;
                self.previouslyTouchedSeat = seat;
            }
        } else {
            self.previouslyTouchedSeat.highlighted = NO;
            self.previouslyTouchedSeat = nil;
        }
    }
    
    [self setNeedsDisplay];
}

- (void)touchesEnded:(NSSet<UITouch*>*)touches withEvent:(UIEvent*)event {
    for (UITouch* touch in touches) {
        CGPoint position = [touch locationInView: self];
        TTSeat* seat = [self seatForPosition: position];
        if (seat != nil) {
            if (seat != self.previouslyTouchedSeat) {
                self.previouslyTouchedSeat.highlighted = NO;
            }
            
            seat.highlighted = NO;
            
            if (seat.selected) {
                seat.selected = NO;
                [self.delegate seatsPlanDidDeselectSeat: seat];
            } else {
                seat.selected = YES;
                [self.delegate seatsPlanDidSelectSeat: seat];
            }
        }
    }
    
    self.previouslyTouchedSeat.highlighted = NO;
    self.previouslyTouchedSeat = nil;
    
    [self setNeedsDisplay];
}

- (void)touchesCancelled:(NSSet<UITouch*>*)touches withEvent:(UIEvent*)event {
    [self touchesEnded:touches withEvent:event];
}

#pragma mark Helpers

- (TTSeat*) seatForPosition:(CGPoint)pos {
    TTSeat* selectedSeat = nil;
    
    // find first
    for (TTSeat* seat in self.seats) {
        CGRect seatRect = CGRectMake(seat.x, seat.y, seat.size.width, seat.size.height);
        
        if (CGRectContainsPoint(seatRect, pos)) {
            if (seat.available) { // seat must be available to be selected
                selectedSeat = seat;
            }
            
            break;
        }
    }
    
    return selectedSeat;
}

- (void) recalculateFrameForSeats:(NSArray<TTSeat*>*)seats texts: (NSArray<TTText*>*)texts {
    // calculate max seats & texts positions
    CGFloat maxY = 0.0, maxX = 0.0;
    for (TTSeat* seat in seats) {
        // x
        if (seat.x > maxX) {
            maxX = seat.x;
        }
        
        // y
        if (seat.y > maxY) {
            maxY = seat.y;
        }
    }
    
    for (TTText* text in texts) {
        // x
        if (text.x > maxX) {
            maxX = text.x;
        }
        
        // y
        if (text.y > maxY) {
            maxY = text.y;
        }
    }
    
    CGRect newFrame = CGRectMake(self.frame.origin.x, self.frame.origin.y,
                                 maxX + kPlanMargins, maxY + kPlanMargins);
    self.frame = newFrame;
}

#pragma mark Changing data

- (void) setSeats:(NSArray<TTSeat *> *)seats {
    _seats = seats;
    
    [self recalculateFrameForSeats: self.seats texts: self.texts];
}

- (void) setTexts:(NSArray<TTText *> *)texts {
    _texts = texts;
    
    [self recalculateFrameForSeats: self.seats texts: self.texts];
}

@end
