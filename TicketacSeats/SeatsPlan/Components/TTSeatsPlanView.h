//
//  TTSeatsPlanView.h
//  TicketacSeats
//
//  Created by Konrad Kołakowski on 05/09/2019.
//  Copyright © 2019 Le Figaro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TTSeat.h"
#import "TTText.h"

#define kPlanMargins 40.0

NS_ASSUME_NONNULL_BEGIN

@protocol TTSeatsPlanViewDelegate <NSObject>

-(void) seatsPlanDidSelectSeat:(TTSeat*)seat;
-(void) seatsPlanDidDeselectSeat:(TTSeat*)seat;

@end

@interface TTSeatsPlanView : UIView

@property (weak, nonatomic) id<TTSeatsPlanViewDelegate> delegate;

@property (strong, nonatomic) NSArray<TTSeat*>* seats;
@property (strong, nonatomic) NSArray<TTText*>* texts;

@end

NS_ASSUME_NONNULL_END
