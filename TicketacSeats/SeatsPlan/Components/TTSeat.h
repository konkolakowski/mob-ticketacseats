//
//  TTSeat.h
//  TicketacSeats
//
//  Created by Konrad Kołakowski on 13/09/2019.
//  Copyright © 2019 Le Figaro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

#define kSeatRectSize 16.0

@interface TTSeat : NSObject

@property (nonatomic, readonly, strong) NSString* seatId;
@property (nonatomic, readonly, strong) NSString* categoryId;

@property (nonatomic, readonly) CGFloat x;
@property (nonatomic, readonly) CGFloat y;
@property (nonatomic, readonly) CGSize size;

@property (nonatomic, readonly) BOOL available;
@property (nonatomic, readonly, strong) NSString* row;
@property (nonatomic, readonly, strong) NSString* number;

@property (nonatomic, readonly, strong) NSString* category;
@property (nonatomic, readonly, strong) NSString* floor;
@property (nonatomic, readonly, strong) NSString* tariffName;
@property (nonatomic, readonly, strong) NSDecimalNumber* price;

@property (nonatomic, readonly, strong) UIColor* color;

@property (nonatomic) BOOL selected;
@property (nonatomic) BOOL highlighted;

-(instancetype) initWithDict:(NSDictionary*)seatDict tariffs:(NSArray*)tariffs;

@end

NS_ASSUME_NONNULL_END
