//
//  TTSelectedSeatsViewController.m
//  TicketacSeats
//
//  Created by Konrad Kołakowski on 05/09/2019.
//  Copyright © 2019 Le Figaro. All rights reserved.
//

#import "TTSelectedSeatsViewController.h"
#import "TTSelectedSeatTableViewCell.h"

#pragma mark Constants & defines

#define kSeatRowHeight 60.0

#define kClosedSelectedSeatsViewHeight 30.0
#define kMaxOpenedSelectedSeatsViewHeight (kSeatRowHeight * 2.5 + kClosedSelectedSeatsViewHeight) // 2.5 because we want to show there's more to scroll

#pragma mark Selected Seats View Controller

@interface TTSelectedSeatsViewController ()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *openButton;
@property (weak, nonatomic) IBOutlet UITableView* seatsTableView;

@property (assign, nonatomic) BOOL opened;
@property (strong, nonatomic) NSLayoutConstraint* heightConstraint;

@end

@implementation TTSelectedSeatsViewController

#pragma mark Initialization & overrides

- (void)viewDidLoad {
    [super viewDidLoad];
 
    // add height constraint
    self.heightConstraint = [self.view.heightAnchor constraintEqualToConstant: kClosedSelectedSeatsViewHeight];
    self.heightConstraint.active = YES;
    
    // setup seats table view
    self.seatsTableView.delegate = self;
    self.seatsTableView.dataSource = self;
    
    [self.seatsTableView registerNib: [UINib nibWithNibName: @"TTSelectedSeatTableViewCell" bundle:nil]
              forCellReuseIdentifier: @"TTSelectedSeatTableViewCell"];
    
    self.opened = NO;
}

#pragma mark Controlling legend display

- (void)open {
    self.opened = YES;
    
    // opened size
    self.heightConstraint.constant = kMaxOpenedSelectedSeatsViewHeight;
    
    [self.view layoutIfNeeded];
    
    // animate open button & blink scroll indicators
    [UIView animateWithDuration: 0.1 animations:^{
        self.openButton.transform = CGAffineTransformMakeRotation( (90.0 * M_PI) / 180.0 );
    }];
    
    [self.seatsTableView flashScrollIndicators];
}

- (void)close {
    self.opened = NO;
    
    self.heightConstraint.constant = kClosedSelectedSeatsViewHeight;
    [self.parentViewController.view layoutIfNeeded];
    
    // animate open button back
    [UIView animateWithDuration: 0.1 animations:^{
        self.openButton.transform = CGAffineTransformMakeRotation( 0.0 );
    }];
}

#pragma mark Controling seats

- (void) setSeats:(NSArray<TTSeat*>*)seats {
    _seats = seats;
    
    NSMutableString* seatsWording = [NSMutableString stringWithString: @"Voir mes places"];
    if (seats.count > 1) {
        [seatsWording appendFormat: @" (%lu séléctionnés)", (unsigned long)seats.count];
    } else {
        [seatsWording appendFormat: @" (%lu séléctionné)", (unsigned long)seats.count];
    }
    self.titleLabel.text = seatsWording;
    
    // refresh seats table view & blink scroll indicators
    [self.seatsTableView reloadData];
    [self.seatsTableView flashScrollIndicators];
}

#pragma mark Actions

- (IBAction)toggleCloseOpen:(id)sender {
    if (self.opened) {
        [self close];
    } else {
        [self open];
    }
}

#pragma mark UITableView delegate & data source

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.seats.count;
}

- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TTSelectedSeatTableViewCell* cell = (TTSelectedSeatTableViewCell*)[tableView dequeueReusableCellWithIdentifier: @"TTSelectedSeatTableViewCell"
                                                                                                      forIndexPath: indexPath];
    if (indexPath.row < self.seats.count) {
        TTSeat* seat = self.seats[indexPath.row];
        [cell fillWithSeat: seat];
    }
    
    return cell;
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return CGFLOAT_MIN;
}

- (CGFloat) tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return CGFLOAT_MIN;
}

@end
