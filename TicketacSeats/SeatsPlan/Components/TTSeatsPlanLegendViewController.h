//
//  TTSeatsPlanLegendViewController.h
//  TicketacSeats
//
//  Created by Konrad Kołakowski on 05/09/2019.
//  Copyright © 2019 Le Figaro. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TTSeatsPlanLegendViewController : UIViewController

@property (nonatomic, strong) NSArray* tariffs;

@end

NS_ASSUME_NONNULL_END
