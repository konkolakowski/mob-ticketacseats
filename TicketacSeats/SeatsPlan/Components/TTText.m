//
//  TTText.m
//  TicketacSeats
//
//  Created by Konrad Kolakowski on 23/09/2019.
//  Copyright © 2019 Le Figaro. All rights reserved.
//

#import "TTText.h"

@interface TTText()

@property (nonatomic) CGFloat x;
@property (nonatomic) CGFloat y;
@property (nonatomic) CGSize size;

@property (nonatomic, strong) NSString* text;

@end

@implementation TTText

-(instancetype) initWithDict:(NSDictionary*)textDict {
    self = [super init];
    if (self) {
        // coordinates
        self.x = [textDict[@"x"] doubleValue];
        self.y = [textDict[@"y"] doubleValue];
        
        // text
        NSString* text = textDict[@"text"];
        if (text != nil && text.length > 0) {
            self.text = text;
        } else {
            self.text = [NSString string];
        }
        
        // size
        self.size = CGSizeMake(kTextRectSize, kTextRectSize);
    }
    
    return self;
}

@end
