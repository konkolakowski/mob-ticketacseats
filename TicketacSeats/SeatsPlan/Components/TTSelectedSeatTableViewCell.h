//
//  TTSelectedSeatTableViewCell.h
//  TicketacSeats
//
//  Created by Konrad Kołakowski on 20/09/2019.
//  Copyright © 2019 Le Figaro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TTSeat.h"

NS_ASSUME_NONNULL_BEGIN

@interface TTSelectedSeatTableViewCell : UITableViewCell

@property (nonatomic, strong) NSString* _Nullable seatId;

- (void) fillWithSeat:(TTSeat*)seat;

@end

NS_ASSUME_NONNULL_END
