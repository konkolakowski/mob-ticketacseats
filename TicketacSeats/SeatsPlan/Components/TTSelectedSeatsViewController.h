//
//  TTSelectedSeatsViewController.h
//  TicketacSeats
//
//  Created by Konrad Kołakowski on 05/09/2019.
//  Copyright © 2019 Le Figaro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TTSeat.h"

NS_ASSUME_NONNULL_BEGIN

@interface TTSelectedSeatsViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) NSArray<TTSeat*>* seats;

@end

NS_ASSUME_NONNULL_END
