//
//  TTText.h
//  TicketacSeats
//
//  Created by Konrad Kolakowski on 23/09/2019.
//  Copyright © 2019 Le Figaro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

#define kTextRectSize 20.0

@interface TTText : NSObject

@property (nonatomic, readonly) CGFloat x;
@property (nonatomic, readonly) CGFloat y;
@property (nonatomic, readonly) CGSize size;

@property (nonatomic, strong, readonly) NSString* text;

-(instancetype) initWithDict:(NSDictionary*)textDict;

@end

NS_ASSUME_NONNULL_END
