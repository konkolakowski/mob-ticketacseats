//
//  TTSeat.m
//  TicketacSeats
//
//  Created by Konrad Kołakowski on 13/09/2019.
//  Copyright © 2019 Le Figaro. All rights reserved.
//

#import "TTSeat.h"

@interface TTSeat()

@property (nonatomic, strong) NSString* seatId;
@property (nonatomic, strong) NSString* categoryId;

@property (nonatomic) CGFloat x;
@property (nonatomic) CGFloat y;
@property (nonatomic) CGSize size;

@property (nonatomic) BOOL available;
@property (nonatomic, strong) NSString* row;
@property (nonatomic, strong) NSString* number;

@property (nonatomic, strong) NSString* category;
@property (nonatomic, strong) NSString* floor;
@property (nonatomic, strong) NSString* tariffName;
@property (nonatomic, strong) NSDecimalNumber* price;

@property (nonatomic, strong) UIColor* color;

@end

@implementation TTSeat

#pragma mark Initialization & overrides

-(instancetype) initWithDict:(NSDictionary*)seatDict tariffs:(NSArray*)tariffs {
    self = [super init];
    if (self) {
        // seat id & category id
        NSString* seatId = seatDict[@"id_seat"];
        if (seatId != nil && seatId.length > 0) {
            self.seatId = seatId;
        } else {
            return nil;
        }
        
        NSString* categoryId = seatDict[@"id_cat"];
        if (categoryId != nil && categoryId.length > 0) {
            self.categoryId = categoryId;
        } else {
            return nil;
        }
        
        // coordinates
        self.x = [seatDict[@"x"] doubleValue];
        self.y = [seatDict[@"y"] doubleValue];
        
        // seat properties & location
        if ([seatDict objectForKey: @"available"] != nil) {
            self.available = [seatDict[@"available"] boolValue];
        } else {
            return nil;
        }
        
        NSString* row = seatDict[@"rang"];
        if (row != nil && row.length > 0) {
            self.row = row;
        } else {
            return nil;
        }
        
        NSString* number = seatDict[@"numero"];
        if (number != nil && number.length > 0) {
            self.number = number;
        } else {
            return nil;
        }
        
        // category & floor
        NSString* category = seatDict[@"denomination"];
        if (category != nil && category.length > 0) {
            self.category = category;
        } else {
            return nil;
        }
        
        NSString* floor = seatDict[@"floor"];
        if (floor != nil && floor.length > 0) {
            self.floor = floor;
        } else {
            return nil;
        }
        
        // match color to tariff
        if (self.available == YES) {
            // find proper tariff
            BOOL tariffFound = NO;
            for (NSDictionary* tariff in tariffs) {
                if ([self.categoryId isEqualToString: tariff[@"cat-id"]]) {
                    self.color = tariff[@"color"];
                    self.tariffName = tariff[@"tarif-name"];
                    self.price = [NSDecimalNumber decimalNumberWithDecimal: [tariff[@"tarif"] decimalValue]];
                    tariffFound = YES;
                    break;
                }
            }
            
            if (!tariffFound) { // if we didn't find it, it's probably not available
                self.color = UIColor.darkGrayColor;
                self.tariffName = nil;
                self.price = [NSDecimalNumber decimalNumberWithString: @"0"];
                self.available = NO;
            }
        } else {
            self.color = UIColor.darkGrayColor;
            self.tariffName = nil;
            self.price = [NSDecimalNumber decimalNumberWithString: @"0"];
        }
        
        self.selected = self.highlighted = NO;
        self.size = CGSizeMake(kSeatRectSize, kSeatRectSize);
    }
    
    return self;
}

- (NSString*) description {
    return [NSString stringWithFormat: @"%@ rang %@ place %@", self.floor, self.row, self.number];
}

- (BOOL) isEqual:(id)object {
    TTSeat* seatObject = (TTSeat*)object;
    
    return [self.seatId isEqualToString: seatObject.seatId];
}

@end
