//
//  TTSeatsPlanLegendViewController.m
//  TicketacSeats
//
//  Created by Konrad Kołakowski on 05/09/2019.
//  Copyright © 2019 Le Figaro. All rights reserved.
//

#import "TTSeatsPlanLegendViewController.h"

#define kClosedLegendHeight 30.0

#define kLegendLineHeight 35.0
#define kLegendLineMargin 8.0

@interface TTSeatsPlanLegendViewController ()

@property (weak, nonatomic) IBOutlet UIButton *openButton;

@property (assign, nonatomic) BOOL opened;
@property (strong, nonatomic) NSLayoutConstraint* heightConstraint;

@end

@implementation TTSeatsPlanLegendViewController

#pragma mark Initialization & overrides
- (void)viewDidLoad {
    [super viewDidLoad];
    
    // add height constraint
    self.heightConstraint = [self.view.heightAnchor constraintEqualToConstant: kClosedLegendHeight];
    self.heightConstraint.active = YES;
    
    // add available tariffs to legend
    UIView* previousView = nil;
    for (NSDictionary* tariff in self.tariffs) {
        UIView* tariffView = [self makeTariffViewWithData: tariff];
        [self.view addSubview: tariffView];
        
        UIView* topAnchorView; NSLayoutYAxisAnchor* targetAnchor; CGFloat topAnchorDistance;
        if (previousView != nil) {
            topAnchorView = previousView;
            targetAnchor = topAnchorView.bottomAnchor;
            topAnchorDistance = kLegendLineMargin;
        } else {
            topAnchorView = self.view;
            targetAnchor = topAnchorView.topAnchor;
            topAnchorDistance = kClosedLegendHeight;
        }
        
        [NSLayoutConstraint activateConstraints: @[
                [tariffView.topAnchor constraintEqualToAnchor: targetAnchor constant: topAnchorDistance],
                [tariffView.leadingAnchor constraintEqualToAnchor: self.view.leadingAnchor],
                [tariffView.trailingAnchor constraintEqualToAnchor: self.view.trailingAnchor],
                
                [tariffView.heightAnchor constraintEqualToConstant: kLegendLineHeight]
            ]
        ];
        
        previousView = tariffView;
    }
    
    self.opened = NO;
}

#pragma mark Helpers
- (UIView*) makeTariffViewWithData:(NSDictionary*)tariff {
    UIView* view = [[UIView alloc] init];
    view.backgroundColor = UIColor.clearColor;
    view.translatesAutoresizingMaskIntoConstraints = NO;
    
    // category label
    UILabel* categoryLabel = [[UILabel alloc] init];
    categoryLabel.translatesAutoresizingMaskIntoConstraints = NO;
    categoryLabel.textColor = tariff[@"color"];
    categoryLabel.text = tariff[@"tarif-name"];
    [view addSubview: categoryLabel];
    
    // price label
    NSDecimalNumber* price = [NSDecimalNumber decimalNumberWithDecimal: [tariff[@"tarif"] decimalValue] ];
    
    UILabel* priceLabel = [[UILabel alloc] init];
    priceLabel.translatesAutoresizingMaskIntoConstraints = NO;
    priceLabel.textColor = UIColor.blackColor;
    priceLabel.text = [NSString stringWithFormat: @"%@€", price];
    [view addSubview: priceLabel];
    
    [NSLayoutConstraint activateConstraints: @[
            [categoryLabel.leadingAnchor constraintEqualToAnchor: view.leadingAnchor constant: 15.0],
            [categoryLabel.centerYAnchor constraintEqualToAnchor: view.centerYAnchor],
            
            [priceLabel.trailingAnchor constraintEqualToAnchor: view.trailingAnchor constant: -15.0],
            [priceLabel.centerYAnchor constraintEqualToAnchor: view.centerYAnchor]
        ]
    ];
    
    return view;
}

#pragma mark Controlling legend display
- (void)open {
    self.opened = YES;
    
    // opened size depends on number of tariffs
    self.heightConstraint.constant = kClosedLegendHeight + kLegendLineHeight * self.tariffs.count
                                                         + kLegendLineMargin * (self.tariffs.count - 1);
    [self.view layoutIfNeeded];
    
    // animate open button
    [UIView animateWithDuration: 0.1 animations:^{
        self.openButton.transform = CGAffineTransformMakeRotation( (90.0 * M_PI) / 180.0 );
    }];
}

- (void)close {
    self.opened = NO;
    
    self.heightConstraint.constant = kClosedLegendHeight;
    [self.parentViewController.view layoutIfNeeded];
    
    // animate open button back
    [UIView animateWithDuration: 0.1 animations:^{
        self.openButton.transform = CGAffineTransformMakeRotation( 0.0 );
    }];
}

#pragma mark Actions
- (IBAction)toggleCloseOpen:(id)sender {
    if (self.opened) {
        [self close];
    } else {
        [self open];
    }
}

@end
