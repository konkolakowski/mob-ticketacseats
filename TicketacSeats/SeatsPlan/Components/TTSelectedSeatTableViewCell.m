//
//  TTSelectedSeatTableViewCell.m
//  TicketacSeats
//
//  Created by Konrad Kołakowski on 20/09/2019.
//  Copyright © 2019 Le Figaro. All rights reserved.
//

#import "TTSelectedSeatTableViewCell.h"

@interface TTSelectedSeatTableViewCell()

@property (weak, nonatomic) IBOutlet UILabel *seatLocationLabel;
@property (weak, nonatomic) IBOutlet UILabel *categoryLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;

@end

@implementation TTSelectedSeatTableViewCell

#pragma mark Initialization & overrides

- (void)awakeFromNib {
    [super awakeFromNib];
    
}

- (void) prepareForReuse {
    [super prepareForReuse];
    
    self.seatId = nil;
}

#pragma mark Filling data

- (void) fillWithSeat:(TTSeat*)seat {
    self.seatId = seat.seatId;
    
    self.seatLocationLabel.text = [seat description];
    self.seatLocationLabel.textColor = seat.color;
    
    self.categoryLabel.text = seat.tariffName;
    
    self.priceLabel.text = [NSString stringWithFormat: @"%@€", seat.price];
}

@end
